# Testsuite

The testsuite consists of a number of executable files, which will print
out the result of each individual test.

Currently the following prerequisites are required:

* /bin/bash
* file
* Perl
* Perl module JSON
* Perl module Archive::ZIP
* Perl module IPC::Run
* Perl module libjson-validator-perl

You can install these with:

```sudo apt install bash file perl libarchive-zip-perl libjson-perl libipc-run-perl libjson-validator-perl```

To generate the JSON schema files,we used `genson` like this:

  find assets/blocktypes -name "*.json" | xargs cat | genson \
  | jq . > test/data/json_schema_blocktypes.json

## Running the tests

Running all tests is done from the main source directory with:

```test/prove```

Running only tests from a certain stage, here the "prebuild" stage:

```prove test/prebuild/*.t```

### Stages

The following stages exist:

#### prebuild

These tests are run **before** the mod-zip-archive is built.

#### postbuild

These tests are run **after** the mod-zip-archive is built.

#### server

Tests starting a server and loading a mod. These tests are run
**after** the mod-zip-archive is built.

## Writing new tests

Testfiles must follow the naming convention of  `STAGE_XXXX_somename.t`,
see above for the different stages. `XXXX` is here the number of
the test in that particular stage. The numbers do not have to be consecutive, but
define the order in which tests are run, e.g. `prebuild_0000_X.t` is run
before `prebuild_0001_Y.t`.

Usually more basic, and fast tests are run first, while slower and more
advanced tests come later.

The tests can be written in any scripting language, provided the first line
contains the correct shebang entry, like ```#!/bin/bash```.

### Output

The output of a testfile consists of the number of tests, and one line
for each individual test. The number of tests can be written as first,
or as last line.

Each test must either output `ok X - Testname`, or `not ok X - Testname`.
While the testname can be arbitray, it is often helpful to include what
was exactly tested, and why and what for.

Here are some example outputs:

```
ok 1 - modinfo.json has data
ok 2 - modinfo.json contains valid JSON
ok 3 - assets/bricklayer/patches/itemtypes-resource-resin.json has data
ok 4 - assets/em/patches/itemtypes-resource-resin.json contains valid JSON
ok 5 - assets/em/patches/blocktypes-clay-bowl.json has data
1..5
```

```
1..3
ok - ./modicon.png is PNG
ok - ./assets/em/textures/block/liquid/glue.png is PNG
ok - ./assets/game/textures/block/liquid/glue.png is PNG
```

And here is an example for a failing test:

```
1..4
ok - ./modicon.png is PNG
ok - ./assets/em/textures/block/liquid/glue.png is PNG
not ok - ./assets/test.png is not PNG but ./assets/test.png: inode/x-empty; charset=binary
ok - ./assets/game/textures/block/liquid/glue.png is PNG
```
