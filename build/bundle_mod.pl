#!/usr/bin/perl -w

# INSTALL
#
#   sudo apt install libarchive-zip-perl libjson-perl
#
# USAGE
#
# perl build/bundle_mod.pl [ TARGET_DIR | MODARCHIVE_FILE ]
#
# SYNOPSIS
#
# Bundles the mod together by taking all files found in all directories
# listed in MANIFEST, and ignoring all files per regexp in IGNORE.

use warnings;
use strict;
use Archive::Zip qw( :ERROR_CODES :CONSTANTS );
use JSON qw/decode_json/;

use constant DEBUG => 1;

# Go to the build path
chdir 'build' if -d 'build';
# And now go one up, so if we started in the build path, we are now
# back at the mod path
chdir '..';

# Define helper routines
sub read_json_file($);
sub add_assets($ $ $ $);
sub ignore_entry($ $);
sub out($);
sub log_debug($ $);
sub bundle_assets($ $ $);

my $modinfo = read_json_file( 'modinfo.json' );
my $zip_base_name = $modinfo->{modid};
if (-f 'build/build_info.json')
  {
  my $build_info = read_json_file( 'build/build_info.json' );
  $zip_base_name = $build_info->{archive_base_name} // $modinfo->{modid};
  }

# handle commandline arguments
my $target = shift;

# formulate a default name
my $name = $zip_base_name . '-' . $modinfo->{version} . '.zip';
# if the target is an existing directory, put the archive into it
if (defined $target && -d $target)
  {
  $target =~ s/\/\z//;	# remove the trailing slash
  $name = "$target/$name";
  }
# if the target was given, but is not a directory, use it as name
elsif (defined $target)
  {
  $name = $target;
  # if the version in the given name does not match, warn
  my $version = $name; $version = $1 if $version =~ /([0-9\.]+)/;
  warn ("Warning: Version mismatch between mod ($modinfo->{version}) and archive ($version)"), sleep(3)
	  unless $version eq $modinfo->{version};
  }
# else: we use the default name set above

out("Reading IGNORE list.");

my $ignore;
open (my $IGNORE, '<', 'build/IGNORE') or warn("Cannot find IGNORE, bundling all assets.");
if ($IGNORE)
  {
  my $line_nr = -1;
  while (my $line = <$IGNORE>)
    {
    $line_nr ++;
    $line =~ s/[\r\n]//g;		# remove line ends
    my $qr;
    eval {
	$qr = qr/$line/;
    };
    if (!$qr)
      {
      warn("Entry '$line' (line $line_nr) is not a valid regexp. Skipping it!\n");
      }
    else
      {
      }
    $ignore->{$line} = qr/$line/	# pre-compile and remember this pattern  
    }
  close $IGNORE;
  }
out("Read " . (scalar keys %$ignore) . " entries.");
  
out("Reading MANIFEST.");
my $assets = {};
open (my $MANIFEST, '<', 'build/MANIFEST') or die ("Cannot read MANIFEST:$!");
while (my $line = <$MANIFEST>)
  {
  $line =~ s/[\r\n]//g;		# remove line ends
  $line =~ s/\/\z//;		# remove a trailing slash
  # if the line is in the form "source => target", then split it up
  my $source = $line;
  my $target = undef;
  if ($line =~ /^(.*) => (.*)/)
    {
    # the name of the sourcefile is different in the final zip
    $source = $1; $target = $2;
    }
  add_assets($assets, $source, $target, 0) unless ignore_entry($source, $ignore);
  }
close $MANIFEST;
out("Found " . (scalar keys %$assets) . " assets to bundle.");

bundle_assets($modinfo, $assets, $name);

out ("All done \\o/");

#############################################################################
# Helper routines

sub read_file($)
  {
  my ($file) = @_;

  open (my $FH, '<', $file) or die ("Cannot read $file: $!");
  binmode $FH, ':utf8';
  local $/ = undef;	# slurp mode
  my $data = <$FH>;	# read all data
  close $FH;

  \$data;		# return a reference to the data
  }

sub read_json_file($)
  {
  my ($file) = @_;
  # read and parse modinfo.json and return it as in-memory hash
  my $json = read_file($file);

  my $info;
  eval {
    $info = decode_json($$json);
  };

  die ("Could not decode JSON: $!") unless ref($info);

  $info;
  }

sub bundle_assets($ $ $)
  {
  my ($modinfo, $assets, $name) = @_;

  out ("Zipping assets.");
  my $zip = Archive::Zip->new();

  for my $file (sort keys %$assets)
    {
    my $target = $assets->{$file};
    if (defined $target)
      {
      log_debug(1, "Adding $file as $target");
      my $file_member = $zip->addFile( $file, $assets->{$file} );
      }
    else
      {
      log_debug(1, "Adding $file");
      my $file_member = $zip->addFile( $file );
      }
    }

  out ("Writing to $name");

  # remove the file if it already exists
  unlink $name if -f $name;

  die ("Could not write zip") if $zip->writeToFileNamed($name) != AZ_OK;

  }

sub add_assets($ $ $ $)
  {
  my ($assets, $src, $dst, $level) = @_;

  $level //= 0;
  if (-f $src)
    {
    $assets->{$src} = $dst;
    }
  elsif (-d $src)
    {
    log_debug(2, (' ' x $level) . "Inspecting $src");
    # read the directory contents
    opendir (my $DH, $src) or die ("Cannot open dir $src: $!"); 
    while (my $entry = readdir($DH))
      {
      next if $entry =~ /^\.\.?$/;			# ignore "." or ".."
      add_assets($assets, "$src/$entry", undef, $level + 1) unless ignore_entry("$src/$entry", $ignore);
      }
    closedir $DH;
    }
  else
    {
    die("$src is neither file nor dir: $!");
    }
  }

sub ignore_entry($ $)
  {
  # Check wether the mentioned asset (dir or file) should be ignored.
  # Returns 1 for yes, 0 for no.
  my ($asset, $ignore) = @_;

  for my $regexp (keys %$ignore)
    {
    return 1 if $asset =~ $ignore->{$regexp};
    }

  # include it
  0;
  }

sub out($)
  {
  my ($msg) = @_;

  print $msg, "\n";
  }

sub log_debug($ $)
  {
  my ($level, $msg) = @_;

  print $msg, "\n" if $level <= DEBUG;
  }

# EOF
