#!/bin/bash

if [ $(cat modinfo.json | jq -r .type) = 'code' ]
then
  RC=`dotnet build`;
  if [ $? != 0 ]
  then
    echo "Error: Code build failed:"
    echo "$RC"
    exit 1;
  fi
  echo "Code was built sucessfully."
  exit 0
else
  echo "Mod is content only, skipping code build."
  exit 0
fi
